<?php

class util
{
    /**
     * Checks if the given value is empty
     * @param $value the value which should be checked
     * @param $minlength
     * @return bool
     */
    function CheckEmpty( $value, $minlength=Null ) {
        if (empty($value)) return false;
        if ( $minlength != Null && strlen($value) < $minlength ) return false;
        else return true;
    }

    /**
     * Checks if the given value is a valid input for a object title
     * @param $value the value which should be checked
     * @param string $empty N = can't be empty, Y = can be empty
     * @return bool
     */
    public static function CheckTitle( $value, $empty='N' ) {
        $pattern_title = '/^[a-zA-ZÄÖÜäöü \-]{2,}$/';
        if ($empty=='Y' && empty($value)) return true;
        if ( preg_match($pattern_title, $value) ) return true;
        else return false;
    }

  public static function CheckDescription( $value, $empty='N' ) {
      $lines = explode("\n", $value);
    $pattern_title = '/^[a-zA-ZÄÖÜäöü \-]{2,}$/';
    if ($empty=='Y' && empty($value))
    {
      return true;
    }
    foreach ($lines as $line )
    {
        if ( preg_match($pattern_title, $line) ) return true;
        else return false;
    }

  }

    public static function CheckFile ($type, $file)
    {
      $valid_extensions = [];
      if($type == 'document')
      {
        $valid_extensions = array("pdf");
      } else if($type == 'picture')
      {
        $valid_extensions = array("jpg","jpeg","png");
      }
      $extensions = pathinfo($file, PATHINFO_EXTENSION);
      if(in_array(strtolower($extensions), $valid_extensions) || empty($file))
      {
        return true;
      }
      else {
        return false;
      }
    }

  public static function CheckCategory( $value, $empty='N' ) {
    $pattern_title = '/^[a-zA-ZÄÖÜäöü \-]{2,}$/';
    if ($empty=='Y' && empty($value)) return true;
    if ( preg_match($pattern_title, $value) ) return true;
    else return false;
  }

    /**
     * Checks if the given value is a number
     * @param $value the number which should be checked
     * @return bool
     */
    public static function isNumber( $value ) {
        if ( !is_numeric($value) ) return false;
        return true;
    }

    /**
     * Checks if the given value is a clean number
     * @param $value the number which should be checked
     * @return bool
     */
    public static function isCleanNumber( $value ) {
        if ( !is_numeric($value) ) return false;
        $pattern_number = '/^[0-9]*$/';
        if ( preg_match($pattern_number, $value) ) return true;
        else return false;
        return true;
    }

}
?>
