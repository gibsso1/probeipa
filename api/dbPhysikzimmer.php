<?php

class dbPhysikzimmer extends db
{
    /**
     * dbKontakte constructor.
     */
    public function __construct()
    {
        self::connect("localhost", "physikzimmer", "root", "");
    }

    /**
     * Inserts a object to the database
     * @param $object the object object to insert
     */
    public function insertObject()
    {

      if( $_REQUEST['editObject'] === 'false' )
      {
        $title = $_REQUEST['title'];
        $description = $_REQUEST['shortdescription'];
        $kid = $_REQUEST['kid'];
        $sql = "INSERT INTO objekt (titel, beschreibung, kid) values (?, ?, ?)";
        $lastInsertedObjectId = parent::queryPrepared($sql, [$title, $description, $kid]);
        $this->uploadFile('picture', $lastInsertedObjectId);
        $this->uploadFile('document', $lastInsertedObjectId);
      }
      else
      {
          if($_REQUEST['editPicture'] === 'true')
          {
            $this->updateFile('picture');
          }
          if($_REQUEST['editDocument'] === 'true')
          {
            $this->updateFile('document');
          }
      }

    }

    public function updateFile($type)
    {
        $oid = $_REQUEST['oid'];
        $oldFileTitle =  $_REQUEST[$type.'_title'];
        $newFile = $_FILES[$type]['name'];
        unlink("upload/".$oldFileTitle);
        $tableName = $type === 'picture' ? 'bild' : 'dokument';
        $sql = "UPDATE " . $tableName . " SET titel = ? WHERE oid = ?";

        if(move_uploaded_file($_FILES[$type]['tmp_name'], "upload/".$newFile))
        {
          if(empty(parent::select("SELECT * FROM " . $tableName ." WHERE oid = ".$oid)))
          {
              parent::queryPrepared("INSERT INTO " . $tableName . "(titel, oid) VALUES (?, ?)", [$newFile, $oid]);
          }else
          {
              parent::queryPrepared($sql, [$newFile, $oid]);
          }
        }
        else {
          parent::queryPrepared("DELETE FROM " .$tableName . " WHERE oid=?", [$oid]);
        }
    }

    public function uploadFile($type, $lastInsertedObjectId)
    {

      $sql = '';
      if($type == 'document')
      {
        $sql = "INSERT INTO dokument (titel, oid) VALUES (?, ?)";
      } else if($type == 'picture')
      {
        $sql = "INSERT INTO bild (titel, oid) VALUES (?, ?)";
      }

      $file = $_FILES[$type]['name'];
      if(move_uploaded_file($_FILES[$type]['tmp_name'], "upload/".$file))
      {
        parent::queryPrepared($sql, [$file, $lastInsertedObjectId]);
      }
    }

    /**
     * Updates a object by the given param and its values
     * @param $object the object object with the values that should be updated
     */
    public function updateObject($objekt)
    {
        $oid = $objekt->oid;
        $titel = $objekt->title;
        $beschreibung = $objekt->description;
        $kid = $objekt->cid;
        $sql = "UPDATE objekt SET titel = ?, beschreibung = ?, kid = ? WHERE oid = ?";
        parent::queryPrepared($sql, [$titel, $beschreibung, $kid, $oid]);
    }

    /**
     * Executes a select-statement with the given search param
     * @param $searchValue the value to be searched for
     * @return array
     */
    public function searchObject($searchValue)
    {
        $results = [];
        $results = parent::selectPrepared("SELECT objekt.*, bild.titel as bild_titel, dokument.titel as dokument_titel, kategorie.kategorie as kategorie_titel FROM objekt
                                        LEFT JOIN bild ON objekt.oid = bild.oid
                                        LEFT JOIN dokument ON objekt.oid = dokument.oid
                                        LEFT JOIN kategorie on objekt.kid = kategorie.kid
                                            WHERE bild.titel LIKE ?
                                            OR dokument.titel LIKE ?
                                            OR objekt.titel LIKE ?
                                            OR objekt.beschreibung LIKE ?
                                            OR kategorie.kategorie LIKE ?", ["%". $searchValue . "%", "%". $searchValue . "%", "%". $searchValue . "%", "%". $searchValue . "%", "%". $searchValue . "%"]);
        return $results;
    }

    public function searchAllObjects()
    {
      return parent::select("SELECT objekt.*, bild.titel as bild_titel, dokument.titel as dokument_titel, kategorie.kategorie as kategorie_titel FROM objekt
                                        LEFT JOIN bild ON objekt.oid = bild.oid
                                        LEFT JOIN dokument ON objekt.oid = dokument.oid
                                        LEFT JOIN kategorie on objekt.kid = kategorie.kid;");
    }

    /**
     * Deletes a object by the given $oid
     * @param $oid The id of the object
     */
    public function deleteObject($oid, $pictureName, $documentName)
    {
        //TODO: Delete foreign kes first (document, picture, booking)
        unlink('upload/'.$pictureName);
        unlink('upload/'.$documentName);
        if (util::isCleanNumber($oid) && $oid != 0)
        {
          parent::queryPrepared("DELETE FROM bild WHERE oid=?", [$oid]);
          parent::queryPrepared("DELETE FROM dokument WHERE oid=?", [$oid]);
          parent::queryPrepared("DELETE FROM reservation WHERE oid=?", [$oid]);
          parent::queryPrepared("DELETE FROM objekt WHERE oid=?", [$oid]);
        }
    }

  public function insertCategory($kategorie)
  {
    $kategorieString = $kategorie->title;
    $sql = "INSERT INTO kategorie (kategorie, parent_kid) VALUES (?, ?)";
    $pkid = null;
    if(!empty($kategorie -> pkid))
    {
      $pkid = $kategorie -> pkid;
    }
    print_r($kategorie);
    parent::queryPrepared($sql, [$kategorieString, $pkid]);
  }

  public function updateCategory( $kategorie )
  {
    $kid = $kategorie->kid;
    $title = $kategorie->title;
    $pkid = $kategorie->pkid;
    $sql = "UPDATE kategorie SET kategorie = ?, parent_kid = ? WHERE kid = ?";
    parent::queryPrepared($sql, [$title, ($pkid == 0 ? null : $pkid), $kid]);
  }

  public function deleteCategory( $kid )
  {
    parent::queryPrepared("DELETE FROM kategorie WHERE kid=?", [$kid]);
  }
  public function searchAllCategories()
  {
    return parent::select("SELECT child.*, parent.kategorie as parent_kategorie FROM kategorie as child LEFT JOIN kategorie as parent ON parent.kid = child.parent_kid");
  }

  public function insertReservation($reservation)
  {
    $reservationFrom = $reservation->reservationFrom;
    $reservationTo = $reservation->reservationTo;
    $oid = $reservation->oid;
    $username = $reservation->username;
    $sql = "INSERT INTO reservation(datumvon, datumbis, oid, username) VALUES ( ?, ?, ?, ?)";
    parent::queryPrepared($sql, [$reservationFrom, $reservationTo, $oid, $username]);
  }

  public function getAllReservations()
  {
      return parent::select("SELECT reservation.*, objekt.titel AS objekt_titel, objekt.beschreibung AS objekt_beschreibung, usernames.username FROM reservation
                                            LEFT JOIN objekt ON reservation.oid = objekt.oid
                                            LEFT JOIN usernames ON reservation.username = usernames.username;");
  }

  public function getBookedOids($reservation)
  {
    $reservationFrom = $reservation->reservationFrom;
    $reservationTo = $reservation->reservationTo;
    return parent::selectPrepared("SELECT oid FROM `reservation` WHERE ( ? <= datumvon AND ? >= datumbis) GROUP BY oid", [$reservationFrom, $reservationTo]);
  }

  public function deleteReservation( $rid )
  {
    parent::queryPrepared("DELETE FROM reservation WHERE rid = ?", [$rid]);
  }

  /**
   * Returns a user depending on his username
   * @param $userData
   * @return array
   */
  public function getUser($userData)
  {
    $sql = "SELECT * FROM usernames WHERE username LIKE ?";
    return parent::selectPrepared($sql, [$userData->username]);
  }

  public function getUserById($mid){
    $sql = "SELECT * FROM usernames WHERE mid LIKE ?";
    return parent::selectPrepared($sql, [$mid]);
  }

  public function getAllUsers()
  {
      return parent::select("SELECT mid, username FROM usernames;");
  }
}

?>
