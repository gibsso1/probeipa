<?php
$dbHelper  = new dbPhysikzimmer();
use ReallySimpleJWT\Token;

class authenticationUtil
{
    const SECRET = 'sec!ReT423*&';
    const EXPIRATION_TIME = 3600;
    const USER = 'localhost';

    /**
     * Checks if the entered password matches with the hashed password from the user in the database.
     * If it matches and a valid token is createt it returns the token which leads to a login.
     * @param $data the data of the request
     * @return string the token
     */
    public static function login($data){
        global $dbHelper;
        $user = $dbHelper->getUser($data->user)[0];
        if(!empty($user)){
            if(password_verify($data->user->password, $user['password'])){
                $token = Token::create($user['mid'], self::SECRET, self::EXPIRATION_TIME + time(), self::USER);
                if( Token::validate($token, self::SECRET) ){
                    return $token;
                }
            }
            else{
                rest::setHttpHeaders(403, true);
            }
        }
    }

    /**
     * Checks if the user in the given token still exists by the userId and if the given token is still valid
     * if both of them are true the same token is returned
     * @param $data the data of the request
     * @return mixed
     */
    public static function authenticate($data){
        global $dbHelper;
        $tokenPayload = Token::getPayload($data->token, self::SECRET);
        if($data->token)
        {
            if( !empty( $dbHelper->getUserById($tokenPayload['user_id'] ) ) && Token::validate($data->token, self::SECRET) )
            {
                return $data->token;
            }
        }
        rest::setHttpHeaders(403, true);
    }
}
?>
