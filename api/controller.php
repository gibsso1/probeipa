<?php
$dbHelper  = new dbPhysikzimmer();

class controller
{

    /**
     * Dispatches the Request based on the params
     * @param $method The HTTP-Method of the request
     * @param $management The management which should be used (land, ort, person)
     * @param $data The data, which should be used (includes search params etc.)
     * @return array
     */
    public static function dispatch($method, $management, $data)
    {
        switch ($management) {
          case 'login':
            return self::login($method, $data);
            case 'objekt':
                return self::objektverwaltung($method, $data);
            case 'kategorie':
                return self::kategorieverwaltung($method, $data);
            case 'versuch':
                return self::versuchsverwaltung($method, $data);
            default:
                rest::setHttpHeaders(405, true);
        }
    }

    /**
     * Login or authenticate the token
     * @param $method The HTTP-method which should be used (POST, PUT, GET, DELETE)
     * @param $data The data that should be used
     * @return mixed|string
     */
    function login($method, $data){
      global $dbHelper;
      switch ($method) {
        case 'POST':
          if($data->token)
          {
            return authenticationUtil::authenticate($data);
          }
          else {
            return authenticationUtil::login($data);
          }
          break;
        case 'GET':
          return $dbHelper->getAllUsers();
      }
    }

    /**
     * Either insert, update, searche or delete a category depending on the given HTTP-method
     * @param $method The HTTP-method which should be used (POST, PUT, GET, DELETE)
     * @param $data The data that should be used
     * @return array
     */
    function kategorieverwaltung($method, $data)
    {
        global $dbHelper;
        switch ($method) {
            case 'POST':
                    self::checkInputCategory($data->kategorie);
                    $dbHelper->insertCategory($data->kategorie);
                    break;
            case 'PUT':
                self::checkInputCategory($data->kategorie);
                $dbHelper->updateCategory($data->kategorie);
                break;
            case 'GET';
                $results = [];
                if(empty($data->kategorie)){
                    $results = $dbHelper->searchAllCategories();
                }
                return $results;
            case 'DELETE':
                $dbHelper->deleteCategory($data->kid);
                break;
        }
    }

    /**
     * Either insert, update, search or delete an objects depending on the given HTTP-method
     * @param $method The HTTP-method which should be used (POST, PUT, GET, DELETE)
     * @param $data The data that should be used
     * @return array
     */
    function objektverwaltung($method, $data)
    {
        global $dbHelper;
        switch ($method) {
            case 'POST':
                self::checkInputObject($data->objekt);
                $dbHelper->insertObject($data);
                break;
          case 'PUT':
                self::checkInputObject($data);
                $dbHelper->updateObject($data);
                break;
            case 'DELETE':
                echo $data->pictureName;
                $dbHelper->deleteObject($data->oid, $data->pictureName, $data->documentName);
                break;
            case 'GET':
              if(empty($data->searchValue)){
                $results = $dbHelper->searchAllObjects();
              }else{
                $results = $dbHelper->searchObject($data -> searchValue);
              }
              return $results;
        }
    }

  /**
   * Either insert, update, search or delete an object depending on the given HTTP-method
   * @param $method The HTTP-method which should be used (POST, PUT, GET, DELETE)
   * @param $data The data that should be used
   * @return array
   */
  function versuchsverwaltung($method, $data)
  {
    global $dbHelper;
    switch ($method) {
      case 'POST':
          self::checkInputReservation( $data );
          $dbHelper->insertReservation($data);
        break;
      case 'PUT':

        break;
      case 'DELETE':
        $dbHelper->deleteReservation($data->rid);
        break;
      case 'GET':
        self::checkInputReservation( $data );
        if(empty($data->reservationFrom) || empty($data->reservationTo))
        {
          return $dbHelper->getAllReservations();
        }else{
          return $dbHelper->getBookedOids($data);
        }
        return null;
    }
  }

    function checkInputCategory($category)
    {
      $status = true;
      $inputfields = array('category' => true);
      if ( !util::CheckCategory( $category->title ) ) {
        $inputfields['category'] = false;
        $status = false;
      }

      if ( !$status ){
        echo rest::encodeJson($inputfields);
        rest::setHttpHeaders(469, true);

      }
    }

    function checkInputObject ($objekt)
    {
      $title = empty($objekt) ? $_REQUEST['title'] : $objekt->title;
      $description =  empty($objekt) ?$_REQUEST['shortdescription'] : $objekt->description;
      $kid =  empty($objekt) ? $_REQUEST['kid'] : $objekt->cid;
      $status = true;
      $inputfields = array('title' => true, 'description' => true, 'category' => true, 'picture' => true, 'document' => true);
      if ( !util::CheckTitle( $title ) ) {
        $inputfields['title'] = false;
        $status = false;
      }
      if ( !util::CheckDescription( $description ) ) {
        $inputfields['description'] = false;
        $status = false;
      }
      if(!util::isNumber($kid))
      {
        $inputfields['category'] = false;
        $status = false;
      }
      if(!util::CheckFile('picture', $_FILES['picture']['name']))
      {
        $inputfields['picture'] = false;
        $status = false;
      }
      if(!util::CheckFile('document', $_FILES['document']['name']))
      {
        $inputfields['document'] = false;
        $status = false;
      }

      if ( !$status ){
        echo rest::encodeJson(empty($objekt));
        rest::setHttpHeaders(469, true);
      }
    }

    function checkInputReservation( $reservation )
    {
      $inputfields = array(reservationFrom => true, reservationTo => true );
      $status = true;
      $reservationFrom = new DateTime($reservation->reservationFrom);
      $reservationTo = new DateTime($reservation->reservationTo);

      if( $reservationTo < $reservationFrom )
      {
        $status = false;
        $inputfields['reservationFrom'] = false;
        $inputfields['reservationTo'] = false;
      }
      if ( !$status ){
        echo rest::encodeJson($inputfields);
        rest::setHttpHeaders(469, true);
      }
    }
}
?>
