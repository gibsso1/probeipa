<?php 
class rest {
	private static $httpVersion = "HTTP/1.1";

    /**
     * Sets the http Headers with the params
     * @param $statusCode the status code of the header
     * @param bool $exit exit after setting the header or not
     * @param string $contentType content type of the header
     */
	public static function setHttpHeaders($statusCode, $exit=false, $contentType="application/json"){
		$statusMessage = self::getHttpStatusMessage($statusCode);
		header(self::$httpVersion. " ". $statusCode ." ". $statusMessage);		
		header("Content-Type:". $contentType);
		if ($exit) exit();
	}

    /**
     * returns a HTTP status Message using the specified status code
     * @param $statusCode the status code of the message which should be returned
     * @return mixed the HTTP status message
     */
	public static function getHttpStatusMessage($statusCode){
		$httpStatus = array(
			100 => 'Continue',  
			101 => 'Switching Protocols',  
			200 => 'OK',
			201 => 'Created',  
			202 => 'Accepted',  
			203 => 'Non-Authoritative Information',  
			204 => 'No Content',  
			205 => 'Reset Content',  
			206 => 'Partial Content',  
			300 => 'Multiple Choices',  
			301 => 'Moved Permanently',  
			302 => 'Found',  
			303 => 'See Other',  
			304 => 'Not Modified',  
			305 => 'Use Proxy',  
			306 => '(Unused)',  
			307 => 'Temporary Redirect',  
			400 => 'Bad Request',  
			401 => 'Unauthorized',  
			402 => 'Payment Required',  
			403 => 'Forbidden',  
			404 => 'Not Found',  
			405 => 'Method Not Allowed',  
			406 => 'Not Acceptable',  
			407 => 'Proxy Authentication Required',  
			408 => 'Request Timeout',  
			409 => 'Conflict',  
			410 => 'Gone',  
			411 => 'Length Required',  
			412 => 'Precondition Failed',  
			413 => 'Request Entity Too Large',  
			414 => 'Request-URI Too Long',  
			415 => 'Unsupported Media Type',  
			416 => 'Requested Range Not Satisfiable',  
			417 => 'Expectation Failed',
			469 => 'Input Validation Failed',
			500 => 'Internal Server Error',  
			501 => 'Not Implemented',  
			502 => 'Bad Gateway',  
			503 => 'Service Unavailable',  
			504 => 'Gateway Timeout',  
			505 => 'HTTP Version Not Supported');
		return ($httpStatus[$statusCode]) ? $httpStatus[$statusCode] : $httpStatus[500];
	}

    /**
     * Encodes the param into a json string
     * @param $responseData tha param which should be encoded into a json
     * @return false|string
     */
	public static function encodeJson($responseData) {
		$jsonResponse = json_encode($responseData);
		return $jsonResponse;		
	}
	
}
?>