<?php

class db
{
    /**
     * @var PDO
     */
    private static $connection;

    /**
     * Creates a connection to the database
     * @param $host hostname
     * @param $database name of the database
     * @param $username username
     * @param $password password
     */
    public static function connect($host, $database, $username, $password)
    {
        if(!self::$connection instanceof PDO){
            self::$connection = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $username, $password);
            self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }

    /**
     * Executes an select statement with the given sql statement
     * @param $sql the sql statement (select) to execute
     * @return array
     */
    public function select($sql)
    {
        return self::$connection->query($sql)->fetchAll();
    }

    /**
     * Executes a prepared statement with the given sql Template and an array of data
     * @param $sqlTemplate The sql statement with placeholder (?)
     * @param $data Array with data
     * @return array
     */
    public function selectPrepared($sqlTemplate, $data)
    {
        $preparedStatement = self::$connection->prepare($sqlTemplate);
        $preparedStatement->execute($data);
        return $preparedStatement->fetchAll();
    }

    /**
     * Executes an SQL query
     * @param $sql the sql query to execute
     * @return string last inserted ID
     */
    public function query($sql)
    {
        self::$connection->exec($sql);
        return self::$connection->lastInsertId();
    }

    /**
     * Executes a prepared query with the given sql Template and an array of data
     * @param $sqlTemplate The sql statement with placeholder (?)
     * @param $data Array with data
     * @return string last inserted ID
     */
    public function queryPrepared($sqlTemplate, $data)
    {
        $preparedStatement = self::$connection->prepare($sqlTemplate);
        $preparedStatement->execute($data);
        return self::$connection->lastInsertId();
    }

    /**
     * places quotes around the input string (if required) and escapes special characters within the input string,
     * using a quoting style appropriate to the underlying driver.
     * @param $value the value to be quoted
     * @return string the quoted String
     */
    public function quote($value)
    {
        return self::$connection -> quote($value);
    }

}

?>
