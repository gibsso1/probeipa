<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');
include_once("util.php");
include("db.php");
include ("dbPhysikzimmer.php");
include_once ("controller.php");
include_once ("rest.php");

// Get requested method
$method = $_SERVER['REQUEST_METHOD'];

// Read post, put, delete data from client
$dataFromClient = json_decode(file_get_contents('php://input'));

// Read get data from client
foreach ( $_GET as $pname => $pvalue ) {
    if ( !$dataFromClient )  $dataFromClient = new stdClass();
    $dataFromClient->$pname = $pvalue;
}

// Run controller
$dataToClient = controller::dispatch( $method, $dataFromClient->management, $dataFromClient );

// Send response to client
rest::setHttpHeaders(200);
echo rest::encodeJson( $dataToClient );
?>
