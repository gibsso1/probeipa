module.exports = {
  lintOnSave: false,
  devServer: {
    proxy: {
      "^/api/": {
        target: "http://localhost:80/probeipa",
        changeOrigin: true //CORS verhindern
      },
      "^/api/": {
        target: "http://localhost:81/probeipa",
        changeOrigin: true //CORS verhindern
      }
    }
  }
};
