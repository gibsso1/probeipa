import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";
Vue.use(VueRouter);
import axios from "axios";
import VueAxios from "vue-axios";
Vue.use(VueAxios, axios);
// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: "active"
});

router.beforeEach((toPage, fromPage, nextPage) => {

  const loginPage = ['/login'];
  const isAuthRequired = !loginPage.includes(toPage.path);
  if ( isAuthRequired && !sessionStorage.getItem('loggedInUser') ) {
    return nextPage('/login');
  }
  else{
    axios(
      {
        url: '/api/login/',
        method: "POST",
        data:{
          token: sessionStorage.getItem('loggedInUser')
        }
      }
    ).catch( error => {
      console.log(error.data);
      sessionStorage.removeItem('loggedInUser');
      return nextPage('/login');
    });
  }

  nextPage();
});

export default router;
