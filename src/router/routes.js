import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Admin pages
const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/objektverwaltung",
    children: [
      {
        path: "objektverwaltung",
        name: "objektverwaltung",
        component: () => import("@/pages/managements/objectmanagement.vue")
      },
      {
        path: "/login",
        name: "login",
        component: () => import("../pages/managements/loginform.vue")
      },
      {
        path: "kategorieverwaltung",
        name: "kategorieverwaltung",
        component: () => import("@/pages/managements/categorymanagement.vue")
      },
      {
        path: "versuchsverwaltung",
        name: "versuchsverwaltung",
        component: () => import("@/pages/managements/experimentmanagement.vue")
      }
    ]
  },
  { path: "*", component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
